import { Items } from './items-def';

export const ITEMS: Items[] = [
  { id: 1,
    name: 'IPhone',
    type: 'Electronics',
    img: 'https://www.kaybectechnology.com/wp-content/uploads/2017/01/iPhone6-Plus-500x500.jpg',
    desc: 'The iPhone 6 and iPhone 6 Plus are smartphones designed and marketed by Apple Inc. The devices are part of the iPhone series and were announced on September 9, 2014, and released on September 19, 2014.[17] The iPhone 6 and iPhone 6 Plus jointly serve as successors to the iPhone 5S and were themselves replaced as flagship devices of the iPhone series by the iPhone 6S and iPhone 6S Plus on September 9, 2015. The iPhone 6 and 6 Plus include larger 4.7 and 5.5 inches (120 and 140 mm) displays, a faster processor, upgraded cameras, improved LTE and Wi-Fi connectivity and support for a near field communications-based mobile payments offering.'
  },
  { id: 2,
    name: 'The Road Not Taken',
    type: 'Book',
    img: 'https://www.kaltura.com/p/2172211/thumbnail/entry_id/1_zu6519qb/def_height/500/def_width/500/',
    desc: '"The Road Not Taken" is a narrative poem. It reads naturally or conversationally, and begins as a kind of photographic depiction of a quiet moment in woods. It consists of four stanzas of 5 lines each. The first line rhymes with the third and fourth, and the second line rhymes with the fifth (a b a a b). The meter is basically iambic tetrameter, with each line having four two-syllable feet. Though in almost every line, in different positions, an iamb is replaced with an anapest. The variation of the rhythm gives naturalness, a feeling of thought occurring spontaneously, and it also affects the reader\'s sense of expectation.'
  },
  { id: 3,
    name: 'PS4',
    type: 'Electronics',
    img: 'https://cdn2.from.ae/media/catalog/product/cache/image/9df78eab33525d08d6e5fb8d27136e95/0/1/01_117_176.jpg',
    desc: 'PlayStation 4 (PS4) is an eighth-generation home video game console developed by Sony Interactive Entertainment. Announced as the successor to the PlayStation 3 during a press conference on February 20, 2013, it was launched on November 15 in North America, November 29 in Europe, South America and Australia; and February 22, 2014, in Japan. It competes with Nintendo\'s Wii U and Switch, and Microsoft\'s Xbox One.'
  },
  { id: 4,
    name: 'XBox',
    type: 'Electronics',
    img: 'https://www.buyondubai.com/template/images/products/14984163921498416385_xbox-x.jpg',
    desc: 'Xbox is a video gaming brand created and owned by Microsoft. It represents a series of video game consoles developed by Microsoft, with three consoles released in the sixth, seventh and eighth generations respectively. The brand also represents applications (games), streaming services, and an online service by the name of Xbox Live. The brand was first introduced in the United States in November 2001, with the launch of the original Xbox console.'
  },
];
