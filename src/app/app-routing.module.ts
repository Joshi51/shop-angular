import { NgModule } from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {ListComponent} from './list/list.component';
import {DetailComponent} from './detail/detail.component';
import {CartItemsComponent} from './cart-items/cart-items.component';

const routes: Routes = [
  { path: '', redirectTo: 'list', pathMatch: 'full' },
  { path: 'list', component: ListComponent},
  { path: 'detail/:id', component: DetailComponent},
  { path: 'cart', component: CartItemsComponent}
];

@NgModule({
  imports: [
    RouterModule.forRoot((routes))
  ],
  exports: [
    RouterModule
  ],
  declarations: []
})
export class AppRoutingModule { }
