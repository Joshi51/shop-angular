import { Component, OnInit } from '@angular/core';
import {ItemsService} from '../items.service';

@Component({
  selector: 'app-cart-items',
  templateUrl: './cart-items.component.html',
  styleUrls: ['./cart-items.component.css']
})
export class CartItemsComponent implements OnInit {
  cartItems: string;
  itemsArr: Array<any> = [];
  itemCount = {};
  constructor(private itemsService: ItemsService) { }

  ngOnInit() {
    this.cartItems = sessionStorage.getItem('cartItems');
    if (this.cartItems) {
      const itemsIds = this.cartItems.split(',').map(function (val) {
        return parseInt(val, 10);
      });
      const allItemsData = this.itemsService.getItems();
      itemsIds.forEach((id) => {
        allItemsData.forEach((oneItem) => {
          if (oneItem.id === id) {
            if (this.itemCount[id]) {
              this.itemCount[id]++;
            } else {
              this.itemCount[id] = 1;
              this.itemsArr.push(oneItem);
            }
          }
        });
      });
    }
  }
}
