import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { Items} from './items-def';
import { ITEMS } from './items-data';

@Injectable({
  providedIn: 'root'
})
export class ItemsService {
  private cartItems = sessionStorage.getItem('cartItems') ? sessionStorage.getItem('cartItems') : {};
  private cartSource = new BehaviorSubject<any>(this.cartItems);

  currentItem = this.cartSource.asObservable();

  getItems(): Items[] {
    return ITEMS;
  }
  getOne(id): Items {
    return ITEMS[id - 1];
  }
  constructor() { }
  sendToCart(detail) {
    this.cartSource.next(detail);
  }
}
