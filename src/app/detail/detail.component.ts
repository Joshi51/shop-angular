import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ItemsService } from '../items.service';

@Component({
  selector: 'app-detail',
  templateUrl: './detail.component.html',
  styleUrls: ['./detail.component.css']
})
export class DetailComponent implements OnInit {
  id: number;
  itemDetail: {};
  constructor(public route: ActivatedRoute, public items: ItemsService) { }

  ngOnInit() {
    this.id = +this.route.snapshot.paramMap.get('id');
    const currObj = this.items.getOne(this.id);
    this.itemDetail = {
      name: currObj.name,
      image: currObj.img,
      type: currObj.type,
      desc: currObj.desc,
      itemId: this.id
    };
  }
  sendToCart() {
    this.items.sendToCart(this.itemDetail);
  }
}
