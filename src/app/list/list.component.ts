import { Component, OnInit } from '@angular/core';
import { ItemsService } from '../items.service';
import {Items} from '../items-def';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css']
})
export class ListComponent implements OnInit {
  items: Items[];

  constructor(private itemServices: ItemsService) { }
  getItems(): void {
    this.items = this.itemServices.getItems();
  }

  ngOnInit() {
    this.getItems();
  }

}
