import {NgModule} from '@angular/core';
import {
  MatBadgeModule, MatButtonModule,
  MatCardModule,
  MatGridListModule,
  MatIconModule,
  MatListModule, MatSidenavModule,
  MatToolbarModule
} from '@angular/material';

@NgModule({
  imports: [
    MatToolbarModule,
    MatCardModule,
    MatListModule,
    MatGridListModule,
    MatIconModule,
    MatBadgeModule,
    MatSidenavModule,
    MatButtonModule
  ],
  exports: [
    MatToolbarModule,
    MatCardModule,
    MatListModule,
    MatGridListModule,
    MatIconModule,
    MatBadgeModule,
    MatSidenavModule,
    MatButtonModule
  ]
})

export class MaterialModules {
 constructor() { }
}
