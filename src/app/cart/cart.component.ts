import { Component, OnInit } from '@angular/core';
import { ItemsService } from '../items.service';

@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.css']
})
export class CartComponent implements OnInit {
  count: number;
  items: Array<any> = [];
  constructor(private itemService: ItemsService) { }

  ngOnInit() {
    const sessionItems = sessionStorage.getItem('cartItems');
    this.count = sessionItems ? sessionItems.split(',').length : 0;
    this.itemService.currentItem.subscribe(items => {
        if (items.name) {
          const currItem = this.items;
          currItem.push(items);
          const currCart = sessionStorage.getItem('cartItems');
          let updateCart;
            if (currCart) {
              updateCart = currCart + ',' + items.itemId;
            } else {
              updateCart = items.itemId;
            }
          sessionStorage.setItem('cartItems', updateCart);
          this.count++;
          console.log(currItem);
        }
      });
  }
}
