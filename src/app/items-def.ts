export interface Items {
  id: number;
  type: string;
  name: string;
  img: string;
  desc: string;
}
